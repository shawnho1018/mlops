## What was in this script:
Install scripts include:

(1) GKE cluster creation: Enable the following features
   * workload identity
   * alias-ip
   * subscribe to rapid channel
   * enable full-accessiblity for GKE (further limited by RBAC)

(2) [KFServing installation](https://github.com/kubeflow/kfserving/blob/master/hack/quick_install.sh): This installation will automatically install the following components:
   * kfserving-system
   * knative-serving
   * istio-system & related monitoring tools
   * cert-manager

## How to use this script?

(1) Prepare installation:
   * Prepare: set project with gcloud
     ```
      gcloud config set project {PROJECT_ID}
     ```
   * Modify two parameters in setup.sh
      * ITERATION_SUFFIX: gke cluster's index
      * Set the loadbalancer IP in patch-file.yaml using the ip information retrieved from the following script:
      ```
      gcloud compute addresses create kfserving --region ${region_name}
      ```

(2) Run script:
```
./setup.sh deploy
```
